//Christoph Ebler 
//MatNr 51411
//24.06.2015

#include <stdio.h>
#include<conio.h>
#include"fileIO.h"
#include"keyfunc.h"
#include"menuefunc.h"
void main (void)
{	char file[20]="Datenbasis.txt";
	STUDENT myStud[150];
	bool exit =false ;
	bool start =false ;
	initZero(myStud);
	start =initDB( file,myStud);
	char menueWahl = '0';
	bool quitAp = false;
	while (start  == true)
	{
		do
		{
			
			 myCls();
			
			printf("Studentenverwaltung 1.22:\n\n");
			fflush(stdin);
			menueWahl =mainMenue ();
			switch (menueWahl){
				case '0':		
					quitAp = true;
					start  = false;
					break;

				case '1':		
					//quitAp = true;
					Eingabe ( file,myStud);
					break;
				case '2':		
					ausgabe (0,myStud,true);
					break;

				case '6':		
				//quitAp = true;
					start  = true;
					quitAp = true;
					start =initDB( file,myStud);

					break;
				case '5':		
					writeFile( file,myStud);
					break;

				default :
					strZerror();
					break;
			}
		}while(quitAp == false);
	}
	
	//_getch();
}