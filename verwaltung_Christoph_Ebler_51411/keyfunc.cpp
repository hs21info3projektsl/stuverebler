//Christoph Ebler 
//MatNr 51411
//24.06.2015
#include<string.h>
#include <stdio.h>
#include<conio.h>
#include"fileIO.h"
#include"menuefunc.h"
#include <stdlib.h>
#include"error_Messages.h"
//#include<>

int find_Free_Space(STUDENT inStud[150])
{
	int nr = 0;
	int zaeler = 0;
	for (zaeler = 0;zaeler<150;zaeler++)
	{
		if (inStud[zaeler].RELEVANT == false)
		{
		nr = zaeler;
		break;
		}
	}
	return zaeler;
}

bool initDB(char filename[20] ,STUDENT initStud[150])
{
	int wahl=0;
	bool retVal = false;
	if (readFile(filename,initStud)!=1)
	{
		printf("Das Oeffnen der Datenbasis war nicht erfolgreich \n\n");
		wahl = OpenDatFailMenue ();
		switch (wahl)
		{
		case 0:

		
			retVal = false;
			break;

		case 1:
		newfile (filename);
		printf("Neue Datenbasis angelegt \n");
		printf("Bitte dr�cken sie eine Beliebige taste um ins Haubtmen� zu gelangen \n");
		_getch();
		retVal = true;
		break;
		default :
			strZerror();
			break;
		
		}
	}
	else 
	{
	retVal = true;
	}
return retVal;
}



void show(int Nr,STUDENT iniStud[150])
	
	{
		if(iniStud[Nr].RELEVANT)
{
	printf("Angaben Zum Studenten :\n");
	printf("\tMatrikelnummer		=>	%s\n",iniStud[Nr].MatNr);
	printf("\tName des Studenten	=>	%s\n",iniStud[Nr].persStudent.Name);
	printf("\tVorname des Studenten	=>	%s\n",iniStud[Nr].persStudent.Vorname);
	printf("\tStartdatum des Studiums	=>	%s\n",iniStud[Nr].beginnDat);
	printf("\tTel NR			=>	%s\n",iniStud[Nr].persStudent.festTel);
	printf("\tMobiel NR		=>	%s\n",iniStud[Nr].persStudent.mobilTel);
	printf("\tEmail Adrese		=>	%s\n",iniStud[Nr].persStudent.eMail);
	printf("\tStrasse			=>	%s\n",iniStud[Nr].adrStudent.strasse);
	printf("\tHaussnummer		=>	%s\n",iniStud[Nr].adrStudent.nr);
	printf("\tPlz			=>	%s\n",iniStud[Nr].adrStudent.plz);
	printf("\tOrt			=>	%s\n",iniStud[Nr].adrStudent.ort);
	

	
	printf("\nAngaben Zur Px Firma :\n");
	printf("\tName			=>	%s\n",iniStud[Nr].pxPartner);
	printf("\tStrasse			=>	%s\n",iniStud[Nr].pxAdresse.strasse);
	printf("\tHaussnummer		=>	%s\n",iniStud[Nr].pxAdresse.nr);
	printf("\tPlz			=>	%s\n",iniStud[Nr].pxAdresse.plz);
	printf("\tOrt			=>	%s\n",iniStud[Nr].adrStudent.ort);

	printf("\nAngaben Zum Bereuer :\n");
	printf("\tName			=>	%s\n",iniStud[Nr].persAnsprech.Name);
	printf("\tVorname			=>	%s\n",iniStud[Nr].persAnsprech.Vorname);
	printf("\tTel NR			=>	%s\n",iniStud[Nr].persAnsprech.festTel);
	printf("\tMobiel NR		=>	%s\n",iniStud[Nr].persAnsprech.mobilTel);
	printf("\tEmail Adrese		=>	%s\n",iniStud[Nr].persAnsprech.eMail);
}
		else 
		{
			 myCls();
			DSnot_Exsiting_error();
			_getch();
		}
	
	// myCls();
}
bool ausgabe (int suchErgebnisse[150],STUDENT iniStud[150],bool all )
{
char wahl = '0';
int nr =0;
int index =0;
bool beenden =false;
	
    do
	{
		myCls();
	printf("Ausgabe der daten\n\n\n");
	show( nr,iniStud);
	fflush(stdin);
	wahl =AusgabeMenue();
	switch (wahl)
	{
		//index out of range ex abfangen 
				case '0':		
					
					beenden  = true;
					break;
				case '4':		
					if (all == true)
					{
						if (nr >0)
						{
							nr--;
						}
						else
						{
							nr=0;
						}
					}
					if (all == false)
					{
					nr = suchErgebnisse[index-1];
					}
					//beenden  = true;
					break;
				case '6':
					if (all == true)
					{
						if (nr <149)
						{
							nr++;
						}
						else
						{
							nr=149;
						}

					}
				if (all == false)
					{
					nr = suchErgebnisse[index+1];
					}
					break;
				default :
					strZerror();
					break;
	}
	
	}while(beenden==false);
	
	return true ;
}

bool Eingabe (char filename[20],STUDENT inStud[150])
	
{  
	int Nr =0;
	bool beenden =false;
	bool saved =false;
	bool nochEinen = true ;
	char wahl ='0';
do{
	myCls();
	saved =false;
	
	
	if (nochEinen ==true )
	{
		Nr=find_Free_Space(inStud);
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	inStud[Nr].RELEVANT = true;
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie einen Matrikelnummer ein :" );
	scanf("%s",inStud[Nr].MatNr);
	}
	fflush(stdin);
	 wahl =EingabeMenue();
	switch (wahl)
	{
		//index out of range ex abfangen 
				case '0':		
					
					beenden  = true;
					break;
				case '1':		
					 saved =true;
					 writeFile(filename,inStud);
					 nochEinen =false;
					break;
				case '2':		
					
					nochEinen =true;
					
					break;

				default :
					strZerror();
					break;

	}
if (beenden ==true&&saved ==false )
{
	if (No_Save_error()==true)
	{
	
	 writeFile(filename,inStud);
	}

}

}while(beenden==false);
	
	return true ;
}

void initZero(STUDENT iniStud[150])
{
	int i  =0;
	for (i=0;i<150;i++)
	{
		//alle werte auf null setzen 
		//diese version ist mit sicherheit nicht die eleganteste aber mir sit so auf die schnelle nichts besseres eingefallen 
		//es funktioniert ja 
		char str[5] = " \0";
		strcpy( iniStud[i].MatNr,str);
		strcpy( iniStud[i].persStudent.Name,str);
		strcpy( iniStud[i].persStudent.Vorname,str);
		strcpy( iniStud[i].persStudent.mobilTel,str);
		strcpy( iniStud[i].persStudent.festTel,str);
		strcpy( iniStud[i].persStudent.eMail,str);
		strcpy( iniStud[i].beginnDat,str);
		strcpy( iniStud[i].adrStudent.strasse,str);
		strcpy( iniStud[i].adrStudent.nr,str);
		strcpy( iniStud[i].adrStudent.plz,str);
		strcpy( iniStud[i].adrStudent.ort,str);
		strcpy( iniStud[i].pxPartner,str);
		strcpy( iniStud[i].pxAdresse.strasse,str);
		strcpy( iniStud[i].pxAdresse.nr,str);
		strcpy( iniStud[i].pxAdresse.plz,str);
		strcpy( iniStud[i].pxAdresse.ort,str);
		strcpy( iniStud[i].persAnsprech.Name,str);
		strcpy( iniStud[i].persAnsprech.Vorname,str);
		strcpy( iniStud[i].persAnsprech.mobilTel,str);
		strcpy( iniStud[i].persAnsprech.festTel,str);
		strcpy( iniStud[i].persAnsprech.eMail,str);
		strcpy( iniStud[i].pxPos,str);
		iniStud[i].RELEVANT=false;

		

	}
}