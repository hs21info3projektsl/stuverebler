//Christoph Ebler 
//MatNr 51411
//24.06.2015


typedef struct 
{
	//dieser wird verwendet um den speicherplatz f�<r name und vorname des studenten und des ansprchpartners zu speichern 
	//da er 2 mal ben�tigt wird hab ich den in einen weiteren typdef ausgelagert 
	char Name[20];
	char Vorname[20];
	char festTel[20];
	char mobilTel[20];
	char eMail[20];
} PERSON;


typedef struct 
{
	char strasse[20];
	char nr[20];
	char plz[20];
	char ort[20];
}ADRESSE;



typedef struct 
{
//Student 
	char MatNr[20];//hier reine zahlen keinen f�<hrnde null m�glich
	PERSON persStudent;
	ADRESSE adrStudent ;
	char beginnDat[20];//hier wurde ein String verwendet da sonst f�hrende nullen abgeschnitten werden 
//PX Patner 
	char pxPartner [20];
	ADRESSE pxAdresse ;
	PERSON persAnsprech;
	char pxPos [20];

	bool RELEVANT ; // ist der datensatz �berhaubt relevant oder soll er als leer betrachtet werden


} STUDENT;

